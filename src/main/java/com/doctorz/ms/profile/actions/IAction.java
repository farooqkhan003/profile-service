package com.doctorz.ms.profile.actions;

import com.doctorz.ms.profile.context.IContext;

/**
 * Created by farooq on 6/30/2018.
 */
public interface IAction<C extends IContext> {
	void apply(C context);
	boolean isApplicable(C context);
}
