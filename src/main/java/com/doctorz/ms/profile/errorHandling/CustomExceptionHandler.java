package com.doctorz.ms.profile.errorHandling;

import com.doctorz.ms.profile.models.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by farooq on 6/30/2018.
 */
@ControllerAdvice(basePackages = "com.doctorz.ms.profile.interfaces")
public class CustomExceptionHandler {
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(RuntimeException.class)
	public ErrorResponse defaultExceptionHandler(HttpServletRequest req, RuntimeException ex){
		//TODO: create the errorType in sdk to have consistency
		ErrorTypeEnum errorTypeEnum = ErrorTypeEnum.SOMETHING_WENT_WRONG;

		ErrorResponse errorResponse = new ErrorResponse(errorTypeEnum);

		//TODO: log the request too.
		return errorResponse;
	}
}
