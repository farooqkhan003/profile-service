package com.doctorz.ms.profile.errorHandling;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * Created by farooq on 6/30/2018.
 */
@Getter
public enum ErrorTypeEnum {
	SOMETHING_WENT_WRONG(0, HttpStatus.BAD_REQUEST.value(), "Something went wrong. Please try again later.");


	private int errorCode;
	private int statusCode;
	private String message;

	ErrorTypeEnum(int errorCode, int statusCode, String message) {
		this.errorCode = errorCode;
		this.statusCode = statusCode;
		this.message = message;
	}



}
