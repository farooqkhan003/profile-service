package com.doctorz.ms.profile.interfaces;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by farooq on 6/30/2018.
 */
@RestController
public class SignUpController extends BaseController {

	@RequestMapping(value = "signup", method = RequestMethod.GET)
	public String signUp(){
		return "healthy";
	}
}
