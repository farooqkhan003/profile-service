package com.doctorz.ms.profile.validators;

import com.doctorz.ms.profile.context.IContext;

/**
 * Created by farooq on 6/30/2018.
 */
public interface IValidator<C extends IContext> {
	void validate(C context);
}
