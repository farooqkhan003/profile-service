package com.doctorz.ms.profile.models;

import com.doctorz.ms.profile.errorHandling.ErrorTypeEnum;
import lombok.Data;

/**
 * Created by farooq on 6/30/2018.
 */
@Data
public class ErrorResponse {
	private Integer errorCode;
	private String message;

	public ErrorResponse(ErrorTypeEnum errorTypeEnum){
		errorCode = errorTypeEnum.getErrorCode();
		message = errorTypeEnum.getMessage();
	}
}
