# profile-service
APIs related to sign, login and user profile

## Contributions
Always work in a new branch and create a `pull request` for `development branch`.

## Branch Promotions
If the build passed all the tests it will be merged to stable and master will be update every fortnightly. 
